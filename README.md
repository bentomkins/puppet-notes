## What is Puppet?

Puppet is a tool that helps system administrators automate the provisioning, 
configuration and management of a server infrastructure.
Planning ahead and using config management tools, like Puppet, can cut down on ~~time 
spent repeating basic tasks~~ doing boring stuff and help ensure that configurations 
are the same.

Puppet comes in two varieties:
1) Puppet Enterprise;
2) Open source Puppet

Both of them run on most Linux distributions, various UNIX platforms, and Windows.

## Advantages
- IaC: Infrastructure as code;
- Consistent version control;
- Downtime reduced;
- Faster development times;
- Automating boring tasks;
- Multi-platform support (Windows, Linux, Unix).

## Client/Server Architecture

```mermaid
    graph RL;
    
    A[Config Repository] --> B[Puppet Master];
    B --> A;
    B --Catalog--> C[Node/Puppet Agent]
    C --Facts--> B;
    B --Catalog--> D[Node/Puppet Agent];
    D --Facts--> B;
    
    linkStyle default stroke-width:1px,fill:none,stroke:light-grey;
```


**Puppet Master**: The node that controls the flow and has the authority;<br>
**Catalog**: A document describing the state of the resources on a node, being managed by Puppet;<br>
**Report**: The actions and infrastructure applied by a catalog, during a Puppet run.<br>

###### Note: SSL encryption is used on all data transmitted.

## Install Puppet VSCode plugin
**Step1**: Install Puppet Development Kit (PDK)
https://puppet.com/try-puppet/puppet-development-kit/

**Step2**: Install the Puppet extension for VSCODE

**Step3**: Open or create a puppet manifest file (*.pp, *.epp, or named Puppetfile)


## Files

#### Attributes:
- Ensure: if the file exists or not, validating what it should be;
- Normal files: source of file, desired content as a string;
- Recursively manage files and delete unmanaged files;
- Symlinks symlink target;
- Others: Backup, checksum, force, ignore, links, replace.

#### Package:
- Manage software packages;
- Ensure if it should be installed or not;
- Present, Latest, Absent, Purged;
- Source: where to obtain the package;
- Provider: which packaging system to use.

#### Service:
- Name: the name of the service to run;
- Ensure-status: is it running or stopped;
- Enable: if it should start on boot;
- Hasrestart: use init script restart for stop+start;
- Hasstatus: to use the init script status command;


## Classes and Modules

## Puppet CLI




## Installing Puppet on Ubuntu 18.xx

